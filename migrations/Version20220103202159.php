<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220103202159 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD COLUMN xero_connections CLOB NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_8D93D649E7927C74');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, email, roles, password, accounts_object, accounts_array, vendors_object, vendors_array FROM "user"');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('CREATE TABLE "user" (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL, accounts_object CLOB DEFAULT NULL --(DC2Type:object)
        , accounts_array CLOB DEFAULT NULL --(DC2Type:array)
        , vendors_object CLOB DEFAULT NULL --(DC2Type:object)
        , vendors_array CLOB DEFAULT NULL --(DC2Type:array)
        )');
        $this->addSql('INSERT INTO "user" (id, email, roles, password, accounts_object, accounts_array, vendors_object, vendors_array) SELECT id, email, roles, password, accounts_object, accounts_array, vendors_object, vendors_array FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
    }
}
