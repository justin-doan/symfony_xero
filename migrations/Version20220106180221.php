<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220106180221 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE organization');
        $this->addSql('ALTER TABLE user ADD COLUMN oauth2 CLOB NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE organization (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, xero_id VARCHAR(255) NOT NULL COLLATE BINARY, tenant_id VARCHAR(255) NOT NULL COLLATE BINARY, name VARCHAR(255) NOT NULL COLLATE BINARY, accounts CLOB DEFAULT NULL COLLATE BINARY --(DC2Type:json)
        , vendors CLOB NOT NULL COLLATE BINARY --(DC2Type:json)
        , customers CLOB NOT NULL COLLATE BINARY --(DC2Type:json)
        )');
        $this->addSql('DROP INDEX UNIQ_8D93D649E7927C74');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, email, roles, password, accounts_array, vendors_array, xero_connections FROM "user"');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('CREATE TABLE "user" (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL, accounts_array CLOB NOT NULL --(DC2Type:json)
        , vendors_array CLOB NOT NULL --(DC2Type:json)
        , xero_connections CLOB NOT NULL --(DC2Type:json)
        )');
        $this->addSql('INSERT INTO "user" (id, email, roles, password, accounts_array, vendors_array, xero_connections) SELECT id, email, roles, password, accounts_array, vendors_array, xero_connections FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
    }
}
