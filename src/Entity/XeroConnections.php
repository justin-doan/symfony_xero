<?php

namespace App\Entity;

use XeroAPI\XeroPHP\Models\Identity\Connection;

class XeroConnections
{
    /** @var Connection[] */
    public $connections;

    public function __construct(array $connections)
    {
        $this->connections = $connections;
    }
}