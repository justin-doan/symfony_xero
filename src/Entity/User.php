<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use XeroAPI\XeroPHP\Models\Accounting\Accounts;
use XeroAPI\XeroPHP\Models\Accounting\Contacts;
use XeroAPI\XeroPHP\Models\Identity\Connection;

/**
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
class User implements UserInterface, PasswordAuthenticatedUserInterface, EquatableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private $email;

    #[ORM\Column(type: 'json')]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    #[ORM\Column(type: 'json')]
    private $accountsArray = [];

    #[ORM\Column(type: 'json')]
    private $vendorsArray = [];

    #[ORM\Column(type: 'json')]
    private $xeroConnections = [];

    #[ORM\Column(type: 'json')]
    private $token = [];

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    #[Pure] public function isEqualTo(UserInterface $user): bool
    {
        return $this->id === $user->getId();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getAccountsArray(): array
    {
        return $this->accountsArray;
    }

    public function setAccountsArray(array $accountsArray): self
    {
        $this->accountsArray = (array)$accountsArray;

        return $this;
    }

    public function getVendorsArray(): array
    {
        return $this->vendorsArray;
    }

    public function setVendorsArray(array $vendorsArray): self
    {
        $this->vendorsArray = (array)$vendorsArray;

        return $this;
    }

    public function getXeroConnections(): array
    {
        return $this->xeroConnections;
    }

    public function setXeroConnections(array $xeroConnections): self
    {

        $this->xeroConnections = $xeroConnections;

        return $this;
    }

    public function getToken(): ?array
    {
        if (empty($this->token)
            || ($this->token['expires'] !== null
                && $this->token['expires'] <= time())
        ) {
            return null;
        }
        return $this->token;
    }

    public function setToken(array $token): self
    {

        $this->token = $token;

        return $this;
    }

    public function getSession()
    {
        return $this->token ?? null;
    }

    public function getAccessToken()
    {
        return $this->token['token'] ?? null;
    }

    public function getRefreshToken()
    {
        return $this->token['refresh_token'] ?? null;
    }

    public function getXeroTenantId()
    {
        return $this->token['tenant_id'] ?? null;
    }

    public function getIdToken()
    {
        return $this->token['id_token'] ?? null;
    }

    public function getHasExpired()
    {
        if (empty($this->token)) {
            if (time() > $this->getExpires()) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public function getExpires()
    {
        return $this->token['expires'] ?? null;
    }
}
