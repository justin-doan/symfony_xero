<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\XeroConnections;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use XeroAPI\XeroPHP\Models\Accounting\Accounts;
use XeroAPI\XeroPHP\Models\Accounting\Contacts;
use function get_class;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface, UserLoaderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function loadUserByIdentifier(string $identifier): ?UserInterface
    {
        $entityManager = $this->getEntityManager();

        return $entityManager->createQuery(
            'SELECT u
                FROM App\Entity\User u
                WHERE u.username = :query
                OR u.email = :query'
        )
            ->setParameter('query', $identifier)
            ->getOneOrNullResult();
    }

    public function updateAccounts(User $user, string $orgID, Accounts $accounts)
    {
        $existing = $user->getAccountsArray() ?? [];
        $values = array();
        foreach ($accounts->getAccounts() as $item) {
            $values[] = [
                "id" => $item->getAccountId(),
                "code" => $item->getCode(),
                "name" => $item->getName(),
                "description" => $item->getDescription(),
            ];
        }
        $existing[$orgID] = $values;
        $user->setAccountsArray($existing);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function updateVendors(User $user, string $orgID, Contacts $vendors)
    {
        $existing = $user->getVendorsArray() ?? [];
        $values = array();
        foreach ($vendors->getContacts() as $item) {
            $values[] = [
                "id" => $item->getContactId(),
                "number" => $item->getPhones()[0]->getPhoneNumber(),
                "email" => $item->getEmailAddress(),
                "name" => $item->getName(),
                "isCustomer" => $item->getIsCustomer() ?? false,
                "isVendor" => $item->getIsSupplier() ?? false,
            ];
        }
        $existing[$orgID] = $values;
        $user->setVendorsArray($existing);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function updateConnections(User $user, XeroConnections $connections)
    {
        $values = array();
        foreach ($connections->connections as $item) {
            $id = $item->getId();
            $values[$id] = [
                "id" => $id,
                "tenantId" => $item->getTenantId(),
                "tenantName" => $item->getTenantName(),
            ];
        }

        $user->setXeroConnections($values);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function updateToken(User $user, $token, $tenantId, $refreshToken, $idToken, $expires = null)
    {
        $user->setToken([
            'token' => $token,
            'expires' => $expires,
            'tenant_id' => $tenantId,
            'refresh_token' => $refreshToken,
            'id_token' => $idToken
        ]);
        $this->_em->persist($user);
        $this->_em->flush();
    }

// /**
//  * @return User[] Returns an array of User objects
//  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
