<?php

namespace App\Controller;

use App\Entity\StorageClass;
use App\Entity\User;
use App\Entity\XeroConnections;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use League\OAuth2\Client\Provider\GenericProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use XeroAPI\XeroPHP\Configuration;
use XeroAPI\XeroPHP\Api\IdentityApi;
use GuzzleHttp\Client;

class ConnectionController extends AbstractController
{

    private GenericProvider $provider;
    private Security $security;
    private User $user;
    private UserRepository $userRepo;

    function __construct(Security $security, ManagerRegistry $doctrine)
    {
        $this->userRepo = new UserRepository($doctrine);
        $this->security = $security;
        if ($security->getUser()) {
            $this->user = $security->getUser();
        } else {
            unset($this->user);
        }
        $this->provider = new GenericProvider([
            'clientId' => '53A8093E32944ABBBEEB1C1164C70D1A',
            'clientSecret' => 'DKBw225jZq0RlJB1oFD_3XU3STzEBUD37_5OTS2F625NeU62',
            'redirectUri' => 'http://localhost:8000/connect/xero/callback',
            'urlAuthorize' => 'https://login.xero.com/identity/connect/authorize',
            'urlAccessToken' => 'https://identity.xero.com/connect/token',
            'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
        ]);
    }

    #[Route('/connect/xero', name: 'xero connection')]
    public function xero(): Response
    {
        if (!isset($this->user)) {
            return $this->redirectToRoute('login');
        }
        return $this->render('connect/xero.html.twig', []);
    }

    #[Route('/connect/xero/auth', name: 'xero auth')]
    public function xeroAuth(): void
    {
        if (!isset($this->user)) {
            $this->redirectToRoute('login');
        }

        ini_set('display_errors', 'On');

        if (session_status() != PHP_SESSION_ACTIVE) {
            session_start();
        }

        // Scope defines the data your app has permission to access.
        // Learn more about scopes at https://developer.xero.com/documentation/oauth2/scopes
        $options = [
            'scope' => ['openid email profile offline_access accounting.settings accounting.settings.read accounting.transactions accounting.contacts accounting.journals.read accounting.reports.read accounting.attachments']
        ];

        // This returns the authorizeUrl with necessary parameters applied (e.g. state).
        $authorizationUrl = $this->provider->getAuthorizationUrl($options);

        // Save the state generated for you and store it to the session.
        // For security, on callback we compare the saved state with the one returned to ensure they match.
        $_SESSION['oauth2state'] = $this->provider->getState();

        // Redirect the user to the authorization URL.
        header('Location: ' . $authorizationUrl);
        exit();
    }

    #[Route('/connect/xero/callback', name: 'xero callback')]
    public function xeroCallback(): void
    {
        if (!isset($this->user)) {
            $this->redirectToRoute('login');
        }

        ini_set('display_errors', 'On');

        // If we don't have an authorization code then get one
        if (!isset($_GET['code'])) {
            echo "Something went wrong, no authorization code found";
            exit("Something went wrong, no authorization code found");

            // Check given state against previously stored one to mitigate CSRF attack
        } elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {
            echo "Invalid State";
            unset($_SESSION['oauth2state']);
            exit('Invalid state');
        } else {

            try {
                // Try to get an access token using the authorization code grant.
                $accessToken = $this->provider->getAccessToken('authorization_code', [
                    'code' => $_GET['code']
                ]);

                $config = Configuration::getDefaultConfiguration()->setAccessToken((string)$accessToken->getToken());
                $identityApi = new IdentityApi(
                    new Client(),
                    $config
                );

                $result = $identityApi->getConnections();

                if (isset($this->user) && isset($this->userRepo)) {
                    $this->userRepo->updateConnections($this->user, new XeroConnections($result));
                }
                // Save my tokens, expiration tenant_id
                $this->userRepo->updateToken(
                    $this->user,
                    $accessToken->getToken(),
                    $result[0]->getTenantId(),
                    $accessToken->getRefreshToken(),
                    $accessToken->getValues()["id_token"],
                    $accessToken->getExpires(),
                );

                header('Location: ' . 'http://localhost:8000/data/xero');
                exit();

            } catch (IdentityProviderException $e) {
                echo "Callback failed";
                exit();
            }
        }
    }
}