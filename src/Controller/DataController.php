<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use XeroAPI\XeroPHP\ApiException;
use XeroAPI\XeroPHP\Configuration;
use XeroAPI\XeroPHP\Api\AccountingApi;
use GuzzleHttp\Client;
use XeroAPI\XeroPHP\Models\Accounting\Accounts;
use XeroAPI\XeroPHP\Models\Accounting\Contacts;
use Symfony\Component\Config\Definition\Exception\Exception;

class DataController extends AbstractController
{
    private static UserRepository $userRepo;
    private Security $security;
    private User $user;
    private Configuration $config;
    private AccountingApi $xeroApi;

    function __construct(Security $security)
    {
        $this->security = $security;
        if ($security->getUser()) {
            $this->user = $security->getUser();
            $this->config = Configuration::getDefaultConfiguration()->setAccessToken($this->user->getAccessToken());
            $this->xeroApi = new AccountingApi(
                new Client(),
                $this->config
            );
        } else {
            unset($this->user);
        }
    }

    #[Route('/data/xero/update', name: 'xero update')]
    public function xeroUpdate(ManagerRegistry $doctrine): Response
    {
        if (!isset($this->user)) {
            return $this->redirectToRoute('login');
        }

        if (!isset(static::$userRepo)) {
            static::$userRepo = new UserRepository($doctrine);
        }

        $forOrg = $_GET['forOrg'];
        $acc = $_GET['acc'] ?? false;
        $vend = $_GET['vend'] ?? false;
        $cust = $_GET['cust'] ?? false;
        $tenantId = $this->user->getXeroConnections()[$forOrg]['tenantId'];

        try {
            $accounts = $this->xeroAccounts($tenantId);
            static::$userRepo->updateAccounts($this->user, $forOrg, $accounts);
        } catch (Exception $e) {
            if ($e->getCode() == 401) {
                return $this->redirectToRoute('xero auth');
            }
        }

        try {
            $vendors = $this->xeroVendors($tenantId);
            static::$userRepo->updateVendors($this->user, $forOrg, $vendors);
        } catch (Exception $e) {
            if ($e->getCode() == 401) {
                return $this->redirectToRoute('xero auth');
            }
        }

        return $this->redirect('/data/xero?forOrg=' . $forOrg . '&acc=' . $acc . '&vend=' . $vend . '&cust=' . $cust);
    }

    public function xeroAccounts(string $tenantId): Accounts
    {
        $order = "Code ASC";

        try {
            $result = $this->xeroApi->getAccounts($tenantId, null, null, $order);
            return $result;
        } catch (ApiException $e) {
            if ($e->getCode() == 401) {
                $this->redirectToRoute('xero connection');
            }
            throw new Exception(`Could not refresh accounts - Exception when calling AccountingApi->getAccounts`, $e->getCode());
        }
    }

    public function xeroVendors(string $tenantId): Contacts
    {
        $order = "Name ASC";

        try {
            return $this->xeroApi->getContacts($tenantId, null, null, $order, null, null, null, false, null);
        } catch (ApiException $e) {
            if ($e->getCode() == 401) {
                $this->redirectToRoute('xero connection');
            }
            throw new Exception(`Could not refresh vendors - Exception when calling AccountingApi->getAccounts`, $e->getCode());
        }
    }

    #[Route('/', name: 'homepage')]
    #[Route('/data/xero', name: 'xero data')]
    public function xero(): Response
    {
        if (!isset($this->user)) {
            return $this->redirectToRoute('login');
        }

        $forOrg = $_GET['forOrg'] ?? null;
        $acc = $_GET['acc'] ?? false;
        $vend = $_GET['vend'] ?? false;
        $cust = $_GET['cust'] ?? false;

        $contactsArray = $this->user->getVendorsArray()[$forOrg] ?? [];
        $vendors = [];
        $customers = [];

        foreach ($contactsArray as $c) {
            $c['isVendor'] == true ? $vendors[] = $c : $customers[] = $c;
        }

        return $this->render('data/xero.html.twig', [
            'user' => $this->user->getUserIdentifier(),
            'accounts' => $this->user->getAccountsArray()[$forOrg] ?? null,
            'vendors' => $vendors,
            'customers' => $customers,
            'orgs' => $this->user->getXeroConnections(),
            'forOrg' => $forOrg,
            'acc' => $acc,
            'vend' => $vend,
            'cust' => $cust,
        ]);
    }

}